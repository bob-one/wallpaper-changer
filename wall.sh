#!/bin/bash
shopt -s nullglob
# Now grab a manly/womanly beer, or a glass of delicious whisk(e)y (from isle of islay),
# set up crontab, startup-script, alias or a hot-key and move on to something more productive TODO: Grab a beer
# Oh, and if you find this script useful, please use it, abuse it, misuse it and preferably make it better
# Best wishes and get well soon, sincerely yours -andre

generate_non_repeating_random_numbers () {
    if [[ ! "$1" = ""  ]] && [[ ! "$2" = "" ]]; then
        awk -v loop=$1 -v range=$2 'BEGIN{
          srand()
          do {
            numb = 1 + int(rand() * range)
            if (!(numb in prev)) {
               print numb
               prev[numb] = 1
               count++
            }
          } while (count<loop)
        }' > $SCRIPT_DATA/random_numbers
    fi }

set_wallpaper () {
    # Setting the wallpaper usin feh
    WALLPAPER=$( awk '{if(NR=="'"$RAND"'") print$0}' $SCRIPT_DATA/list_of_wallpapers)
    feh --bg-scale $WALLPAPER 2>>$SCRIPT_DATA/wallpaper_error
    exit }

start_over () {
    # reset INDX and RAND
    INDX=1; RAND=1
    echo $INDX > $SCRIPT_DATA/current-index
    
    # Get the number of jpg or png in the given wallpaper folder
    NUMJPGS=($WALLPAPER_PATH/*.jpg)
    NUMJPGS=${#NUMJPGS[@]}
    NUMPNGS=($WALLPAPER_PATH/*.png)
    NUMPNGS=${#NUMPNGS[@]}
    NUMPICS=$(($NUMJPGS + $NUMPNGS +1))
    echo $NUMPICS > $SCRIPT_DATA/number_of_pics # For use in the main loop
    
    # Generate the random numbers equal to the number of pictures in the wallpaper folder, without repeating any numbers
    generate_non_repeating_random_numbers $NUMPICS $NUMPICS

    # Create a list of all the filenames with path and save to a file
    echo "" > $SCRIPT_DATA/list_of_wallpapers
    for f in "$WALLPAPER_PATH"/*
    do
        echo "$f" >> $SCRIPT_DATA/list_of_wallpapers
    done
    set_wallpaper $RAND }

cycle_through () {
    # Increment through the list of indexes to set the wallpapers each time the script is run
    INDX=$(( INDX+1 ))
    echo $INDX > $SCRIPT_DATA/current-index
    RAND=$( head -n 1 $SCRIPT_DATA/random_numbers )
    sed -i -e "1d" $SCRIPT_DATA/random_numbers
    set_wallpaper $RAND }

# Standard check in allmost all of my scripts, if sudo for some reason is needed, then remove this check
[[ $UID == 0 ]] && ( echo "Dont run an unknown script as sudo, atleast read through it, and try to understand it before you try sudo on it"; exit )

# Get the path to where this script is run from and asigning it the the variable $SCRIPT_PATH
# In case it's a symlink: Resolve $SOURCE until it's no longer a symlink
# If $SOURCE was a relative symlink, resolve it relative to the path where the symlink was located
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
    SCRIPT_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$SCRIPT_PATH/$SOURCE"
done
SCRIPT_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DATA="$SCRIPT_PATH/data"

# Create neccesary files, for storing values for later use, if they don't exists
[[ ! -d "$SCRIPT_DATA" ]] && ( mkdir $SCRIPT_PATH/data )
[[ ! -f "$SCRIPT_DATA/current-index" ]] && ( touch $SCRIPT_DATA/current-index; echo "reset" > $SCRIPT_DATA/current-index )
[[ ! -f "$SCRIPT_DATA/random_numbers" ]] && ( touch $SCRIPT_DATA/random_numbers )
[[ ! -f "$SCRIPT_DATA/list_of_wallpapers" ]] && ( touch $SCRIPT_DATA/list_of_wallpapers )
[[ ! -f "$SCRIPT_DATA/number_of_pics" ]] && ( touch $SCRIPT_DATA/number_of_pics )
[[ ! -f "$SCRIPT_DATA/wallpaper_error" ]] && ( touch $SCRIPT_DATA/wallpaper_error )

# Cheking if the path to the wallpaperfolder is given as an argument
[[ "$1" = "" ]] && ( echo -e " Please add location to your wallpaper folder."; exit )
[[ ! "$1" = "" ]] && WALLPAPER_PATH="$1"

# Set PATH variable to be able to use crontab to set the wallpaper
export DISPLAY=:0
export XAUTHORITY=$HOME/.Xauthority
export XDG_RUNTIME_DIR=/run/user/1000

# If something goes wrong, just replace the contents of the file "current-index" with "reset"
read INDX < $SCRIPT_DATA/current-index 2>>$SCRIPT_DATA/wallpaper_error
[[ "$INDX" = "reset" ]] && ( start_over )
read LIMIT < $SCRIPT_DATA/number_of_pics 2>>$SCRIPT_DATA/wallpaper_error
[[ $INDX -eq $LIMIT ]] && ( start_over )
[[ $INDX -lt $LIMIT ]] && ( cycle_through )
